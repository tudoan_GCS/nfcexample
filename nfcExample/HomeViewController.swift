//
//  HomeViewController.swift
//  nfcExample
//
//  Created by gcs test on 9/8/20.
//

import UIKit
import CoreNFC
import zonar_NFCScanner_ios

class HomeViewController: UIViewController {
    @IBOutlet weak var nfcLabel: UILabel!
    @IBOutlet weak var scanNFCBtn: UIButton!
    
    var nfcSession: NFCNDEFReaderSession?
    let zn = ZNScanner()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        validateNFCAvailability()
        
    }
    
    private func validateNFCAvailability() {
        guard NFCNDEFReaderSession.readingAvailable else {
            let alertController = UIAlertController(
                title: "Scanning Not Supported",
                message: "This device doesn't support tag scanning.",
                preferredStyle: .alert
            )
            
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            scanNFCBtn.isHidden = true
            return
        }
    }
    
    @IBAction func startScan(_ sender: Any) {
        self.nfcSession = NFCNDEFReaderSession(delegate: self, queue: nil, invalidateAfterFirstRead: true)
        self.nfcSession?.alertMessage = "Hold near to scan"
        self.nfcSession?.begin()
        
//        let alertController = UIAlertController(title: "NFC scanner alert",
//                                                message: "Choose scan NFC tag or report an issue",
//                                                    preferredStyle: .actionSheet)
//
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
//            print("Cancel scan tag")
//        })
//        cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
//        
//        [
//            UIAlertAction(title: "Scan", style: .default , handler:{ (UIAlertAction) in
//                print("Start scanning")
//                self.nfcSession = NFCNDEFReaderSession(delegate: self, queue: nil, invalidateAfterFirstRead: true)
//                self.nfcSession?.alertMessage = "Hold near to scan"
//                self.nfcSession?.begin()
//            }),
//            
//            UIAlertAction(title: "Report an issue", style: .default , handler:{ (UIAlertAction)in
//                print("Report an issue for NFC tag")
//            }),
//            
//            cancelAction
//        ].forEach {
//            alertController.addAction($0)
//        }
//        
//        self.present(alertController, animated: true, completion: {
//            print("Scanning complete")
//        })
    }
    
}

extension HomeViewController: NFCNDEFReaderSessionDelegate {
    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
//        guard
//            let readerError = error as? NFCReaderError,
//            readerError.code != .readerSessionInvalidationErrorFirstNDEFTagRead,
//            readerError.code != .readerSessionInvalidationErrorUserCanceled else {
//            return
//        }
//
//        let alertController = UIAlertController(
//            title: "Session Invalidated",
//            message: error.localizedDescription,
//            preferredStyle: .alert
//        )
//
//        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        guard let alertController = zn.errorAlertController(for: session, error: error) else {
            return
        }

        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
//        session.alertMessage = "Found 1 NDEF message"
        var result = ""
        for payload in messages[0].records {
            result += String.init(data: payload.payload.advanced(by: 3), encoding: .utf8)!
        }
        
        let res2 = zn.assetTag(from: session, messages: messages) ?? 0
        
        print("result = \(result)")
        DispatchQueue.main.async {
            self.nfcLabel.text = result + " - res2 = \(res2)"
        }
    }
    
//    @available(iOS 13.0, *)
//    func readerSession(_ session: NFCNDEFReaderSession, didDetect tags: [NFCNDEFTag]) {
//        print("tags = \(tags)")
//    }
    
}
